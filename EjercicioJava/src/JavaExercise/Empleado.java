package JavaExercise;

public class Empleado {

    private int ID;
    private int age;
    private String name;
    private String address;
    private int salary;
    private int bono;

    public Empleado() {

    }

    public Empleado(int ID, int age, String name, String address, int salary) {
        this.ID = ID;
        this.age = age;
        this.name = name;
        this.address = address;
        this.salary = salary;
        this.bono = this.salary > 50000 ? 10000 : 5000;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getBono() {
        return bono;
    }

    public void setBono(int bono) {
        this.bono = bono;
    }

    public String toString() {

        return "ID: " + ID + " Age:" + age + " Name:" + name + " Address:" + address + " Salary:" + salary + " Bono:" + bono;

    }

}
