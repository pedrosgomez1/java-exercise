package JavaExercise;

public interface IActividades {

    void actividadActual();

    default void disenoVistaWeb(){
        System.out.print("diseño web");
    }

    default void mantenimientoAplicacion(){
        System.out.print("mantenimiento app");
    }

    default void resolverTickets(){
        System.out.print("resolucion de tickets");
    }

}
