package JavaExercise;

import java.util.List;

public class IngenieroSoporte extends Empleado implements IActividades {

    private List<String> tecnologiasSoporte;
    private int tiempoDeExperiencia;

    public IngenieroSoporte() {
    }

    public IngenieroSoporte(int ID, int age, String name, String address, int salary, List<String> tecnologiasSoporte, int tiempoDeExperiencia) {
        super(ID, age, name, address, salary);
        this.tecnologiasSoporte = tecnologiasSoporte;
        this.tiempoDeExperiencia = tiempoDeExperiencia;
    }

    public List<String> getTecnologiasSoporte() {
        return tecnologiasSoporte;
    }

    public void setTecnologiasSoporte(List<String> tecnologiasSoporte) {
        this.tecnologiasSoporte = tecnologiasSoporte;
    }

    public int getTiempoDeExperiencia() {
        return tiempoDeExperiencia;
    }

    public void setTiempoDeExperiencia(int tiempoDeExperiencia) {
        this.tiempoDeExperiencia = tiempoDeExperiencia;
    }

    @Override
    public void actividadActual() {
        System.out.println("Ingeniero en Soporte.");
    }

    @Override
    public void resolverTickets() {
        System.out.println("Realiza la resolucion de los tickets.");
    }

    @Override
    public String toString()
    {
        return "Tecnologias Soporte:" + tecnologiasSoporte + " Tiempo de experiencia:" + tiempoDeExperiencia;
    }

}
