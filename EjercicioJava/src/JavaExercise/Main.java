package JavaExercise;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<String> languages = new ArrayList<String>();
        languages.add("Java");
        languages.add("Python");

        List<String> frames = new ArrayList<String>();
        frames.add("Laravel");
        frames.add("CodeIgniter");

        List<String> tecWeb = new ArrayList<String>();
        tecWeb.add("Bootstrap");
        tecWeb.add("Dreamweaver");

        List<String> tecMov = new ArrayList<String>();
        tecMov.add("Android");
        tecMov.add("IOS");

        List<String> tecSop = new ArrayList<String>();
        tecSop.add("Team Viewer");
        tecSop.add("Anydesk");

        Empleado e1 = new Empleado((int)Math.floor(Math.random()*(50000-10000+1)+10000), 33, "Diego", "Fracc. Parras", (int)Math.floor(Math.random()*(80000-10000+1)+10000));
        Programador p1 = new Programador((int)Math.floor(Math.random()*(50000-10000+1)+10000), 50, "Pablo", "Col. Constitucion", (int)Math.floor(Math.random()*(80000-10000+1)+10000), languages, frames, "Programador");
        ProgramadorWeb pw1 = new ProgramadorWeb((int)Math.floor(Math.random()*(50000-10000+1)+10000), 35, "Samuel", "Villa Teresa", (int)Math.floor(Math.random()*(80000-10000+1)+10000), languages, frames, "Programdor Web", tecWeb, (int)Math.floor(Math.random()*(15-1+1)+1));
        ProgramadorMobile pm1 = new ProgramadorMobile((int)Math.floor(Math.random()*(50000-10000+1)+10000), 42, "Emanuel", "Gremial", (int)Math.floor(Math.random()*(80000-10000+1)+10000), languages, frames, "Programador Movil", tecMov, (int)Math.floor(Math.random()*(15-1+1)+1));
        IngenieroSoporte is1 = new IngenieroSoporte((int)Math.floor(Math.random()*(50000-10000+1)+10000), 27, "Francisco", "Hadas", (int)Math.floor(Math.random()*(80000-10000+1)+10000), tecSop,(int)Math.floor(Math.random()*(15-1+1)+1));

        System.out.println(e1);
        System.out.println(p1);
        System.out.println(pw1);
        System.out.println(pm1);
        System.out.println(is1);

        //------------------ TESTS ---------------------

        //Test 1
        //Programador programador = new Empleado(); No es posible

        //Test 2
        //ProgramadorWeb programadorWeb = new Empleado(); No es posible

        //Test 3
        //ProgramadorMobile programadorMobile = new Empleado(); No es posible

        //Test 4
        //IngenieroSoporte ingenieroSoporte = new Empleado(); No es posible

        //Test 5
        Empleado empleado = new Programador(); // Si es posible

        //Test 6
        Empleado empleado1 = new ProgramadorWeb(); // Si es posible

        //Test 7
        Empleado empleado2 = new ProgramadorMobile(); // Si es posible

        //Test 8
        Empleado empleado3 = new IngenieroSoporte(); // Si es posible

        //Test 9
        //ProgramadorWeb programadorWeb = new Programador(); No es posible

        //Test 10
        //ProgramadorMobile programadorMobile = new Programador(); No es posible

        //Test 11
        Programador programador = new ProgramadorWeb(); // Si es posible

        //Test 12
        Programador programador1 = new ProgramadorMobile(); // Si es posible

    }

}
