package JavaExercise;

import java.util.ArrayList;
import java.util.List;

public class Programador extends Empleado {

    private List<String> lenguajes;
    private List<String> frameworks;
    private String tituloProfesional;

    public Programador() {
    }

    public Programador(int ID, int age, String name, String address, int salary, List<String> lenguajes, List<String> frameworks, String tituloProfesional) {
        super(ID, age, name, address, salary);
        this.lenguajes = lenguajes;
        this.frameworks = frameworks;
        this.tituloProfesional = tituloProfesional;
    }

    public List<String> getLenguajes() {
        return lenguajes;
    }

    public void setLenguajes(List<String> lenguajes) {
        this.lenguajes = lenguajes;
    }

    public List<String> getFrameworks() {
        return frameworks;
    }

    public void setFrameworks(List<String> frameworks) {
        this.frameworks = frameworks;
    }

    public String getTituloProfesional() {
        return tituloProfesional;
    }

    public void setTituloProfesional(String tituloProfesional) {
        this.tituloProfesional = tituloProfesional;
    }

    public void imprimirLenguajes() {
        ((ArrayList)lenguajes).forEach(lenguaje -> System.out.print(lenguaje + ", "));
        System.out.println();
    }

    public void imprimirFrameworks() {
        ((ArrayList)frameworks).forEach(framework -> System.out.print(framework + ", "));
        System.out.println();
    }

    @Override
    public String toString()
    {
        return "Lenguajes:" + lenguajes + " Frameworks:" + frameworks + " Titulo Profesional:" + tituloProfesional;
    }

}
