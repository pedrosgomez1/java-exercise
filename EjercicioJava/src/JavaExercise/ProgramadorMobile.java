package JavaExercise;

import java.util.ArrayList;
import java.util.List;

public class ProgramadorMobile extends Programador implements IActividades {

    private List<String> tecnologiasMoviles;
    private int tiempoDeExperiencia;

    public ProgramadorMobile() {
    }

    public ProgramadorMobile(int ID, int age, String name, String address, int salary, List<String> lenguajes, List<String> frameworks, String tituloProfesional, List<String> tecnologiasMoviles, int tiempoDeExperiencia) {
        super(ID, age, name, address, salary, lenguajes, frameworks, tituloProfesional);
        this.tecnologiasMoviles = tecnologiasMoviles;
        this.tiempoDeExperiencia = tiempoDeExperiencia;
    }

    public List<String> getTecnologiasMoviles() {
        return tecnologiasMoviles;
    }

    public void setTecnologiasMoviles(List<String> tecnologiasMoviles) {
        this.tecnologiasMoviles = tecnologiasMoviles;
    }

    public int getTiempoDeExperiencia() {
        return tiempoDeExperiencia;
    }

    public void setTiempoDeExperiencia(int tiempoDeExperiencia) {
        this.tiempoDeExperiencia = tiempoDeExperiencia;
    }

    public void imprimirTecnologiasMoviles() {
        ((ArrayList)tecnologiasMoviles).forEach(tecnologiaMoviles -> System.out.print(tecnologiaMoviles + ", "));
        System.out.println();
    }

    @Override
    public void actividadActual() {
        System.out.println("Programacion Mobile.");
    }

    @Override
    public void mantenimientoAplicacion() {
        System.out.println("Realiza el mantenimiento de la aplicacion.");
    }

    @Override
    public String toString()
    {
        return "Tecnologias Moviles:" + tecnologiasMoviles + " Tiempo de experiencia:" + tiempoDeExperiencia;
    }

}
