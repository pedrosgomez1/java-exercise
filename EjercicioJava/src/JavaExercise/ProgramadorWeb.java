package JavaExercise;

import java.util.ArrayList;
import java.util.List;

public class ProgramadorWeb extends Programador implements IActividades {

    private List<String> tecnologiasWeb;
    private int tiempoDeExperiencia;

    public ProgramadorWeb() {

    }

    public ProgramadorWeb(int ID, int age, String name, String address, int salary, List<String> lenguajes, List<String> frameworks, String tituloProfesional, List<String> tecnologiasWeb, int tiempoDeExperiencia) {
        super(ID, age, name, address, salary, lenguajes, frameworks, tituloProfesional);
        this.tecnologiasWeb = tecnologiasWeb;
        this.tiempoDeExperiencia = tiempoDeExperiencia;
    }

    public List<String> getTecnologiasWeb() {
        return tecnologiasWeb;
    }

    public void setTecnologiasWeb(List<String> tecnologiasWeb) {
        this.tecnologiasWeb = tecnologiasWeb;
    }

    public int getTiempoDeExperiencia() {
        return tiempoDeExperiencia;
    }

    public void setTiempoDeExperiencia(int tiempoDeExperiencia) {
        this.tiempoDeExperiencia = tiempoDeExperiencia;
    }

    public void imprimirTecnologiasWeb() {
        ((ArrayList)tecnologiasWeb).forEach(tecnologiaWeb -> System.out.print(tecnologiaWeb + ", "));
        System.out.println();
    }

    @Override
    public void actividadActual() {
        System.out.println("Programacion Web.");
    }

    @Override
    public void disenoVistaWeb() {
        System.out.println("Realiza el diseño de la pagina web.");
    }

    @Override
    public String toString()
    {
        return "Tecnologias Web:" + tecnologiasWeb + " Tiempo de experiencia:" + tiempoDeExperiencia;
    }

}
